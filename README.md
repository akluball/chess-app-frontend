# Chess Application Frontend

This is the frontend of the [chess-application].

This project requires jdk8 and npm/node.js

## Format Source (Prettier)

```
./gradlew prettier
```

## Build

```
./gradlew war
```

The application expects the following variables set in the build environment:
1. `CHESSAPP_FRONTEND_DEPLOYMENTNAME`
2. `CHESSAPP_GAMEHISTORY_URI`
3. `CHESSAPP_GAMESTATE_URI`
4. `CHESSAPP_MESSAGING_URI`
5. `CHESSAPP_USER_URI`

[chess-application]: https://gitlab.com/akluball/chess-app.git
