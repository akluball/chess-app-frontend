export interface MessageDto {
    userId: number;
    text: string;
    epochSeconds: number;
}

export interface ConversationDto {
    id: number;
    converserIds: number[];
    messages: MessageDto[];
}

export interface GameConversationDto {
    whiteSideUserId: number;
    blackSideUserId: number;
    messages: MessageDto[];
}
