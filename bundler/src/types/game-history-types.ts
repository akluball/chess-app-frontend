import { MoveSummaryDto } from './game-state-types';

export enum ResultTag {
    IN_PROGRESS = '*',
    WHITE_WINS = '1-0',
    BLACK_WINS = '0-1',
    DRAW = '1/2-1/2'
}

export enum TerminationTag {
    UNTERMINATED = 'unterminated',
    NORMAL = 'normal',
    TIME_FORFEIT = 'time-forfeit'
}

export enum SourceDisambiguation {
    NONE = 'none',
    FILE = 'file',
    RANK = 'rank',
    BOTH = 'both'
}

export interface GameHistoryDto {
    id: number;
    whiteSideUserId: number;
    blackSideUserId: number;
    startEpochSeconds: number;
    whiteWinsRatingAdjustment: number;
    whiteLosesRatingAdjustment: number;
    whiteDrawsRatingAdjustment: number;
    millisPerPlayer: number;
    moveCount: number;
    moveSummaries: MoveSummaryDto[];
    endEpochSeconds: number;
    resultTag: string;
    terminationTag: string;
}

export interface DirectGameRequestDto {
    id: number;
    requesterId: number;
    requesteeId: number;
    epochSeconds: number;
}

export interface StatsDto {
    winCount: number;
    lossCount: number;
    drawCount: number;
}

export function createStatsDtoPlaceholder(): StatsDto {
    return { winCount: 0, lossCount: 0, drawCount: 0 };
}

export interface StatsContainerDto {
    whiteSideStats: StatsDto;
    blackSideStats: StatsDto;
}

export function createStatsContainerDtoPlaceholder(): StatsContainerDto {
    return {
        whiteSideStats: createStatsDtoPlaceholder(),
        blackSideStats: createStatsDtoPlaceholder()
    };
}
