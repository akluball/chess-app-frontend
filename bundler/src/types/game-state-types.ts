export interface MoveDto {
    pieceLetter: string;
    promotionPieceLetter: string;
    source: string;
    target: string;
}

export interface MoveSummaryDto {
    pieceLetter: string;
    promotionPieceLetter: string;
    source: string;
    target: string;
    isCapture: boolean;
    sourceDisambiguation: string;
    durationMillis: number;
    gameStatus: string;
}

export interface MoveResultDto {
    wasExecuted: boolean;
    whiteSideMillisRemaining: number;
    blackSideMillisRemaining: number;
    moveSummary: MoveSummaryDto;
}

export interface GameStateDto {
    gameHistoryId: number;
    whiteSideUserId: number;
    blackSideUserId: number;
    moveSummaries: MoveSummaryDto[];
    positionToPiece: { [index: string]: string };
    whiteSideMillisRemaining: number;
    blackSideMillisRemaining: number;
}
