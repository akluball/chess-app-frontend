export interface UserDto {
    id: number;
    handle: string;
    rating: number;
    firstName: string;
    lastName: string;
    joinEpochSeconds: number;
}

export function createUserDtoPlaceholder(): UserDto {
    return {
        id: 0,
        handle: '',
        rating: 0,
        firstName: '',
        lastName: '',
        joinEpochSeconds: 0
    };
}
