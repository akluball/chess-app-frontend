import axios from 'axios';
import store from '../store';
import { GAMESTATE_URI } from '../env-config';
import { GameStateDto } from '../types/game-state-types';

const gamestateApi = `${GAMESTATE_URI}/api/gamestate`;

export async function getGameState(gameStateId: number): Promise<GameStateDto> {
    const res = await axios.get(
        `${gamestateApi}/${gameStateId}`,
        store.getters.bearerMixin()
    );
    return res.data as GameStateDto;
}

export async function getInProgressGames(
    userId?: number
): Promise<GameStateDto[]> {
    let uri = gamestateApi;
    if (userId !== undefined) {
        uri += `?userId=${userId}`;
    }
    const res = await axios.get(uri, store.getters.bearerMixin());
    return res.data;
}

export async function getMyMoveGames(userId?: number): Promise<GameStateDto[]> {
    let uri = `${gamestateApi}?isTurn=true`;
    if (userId !== undefined) {
        uri += `&userId=${userId}`;
    }
    const res = await axios.get(uri, store.getters.bearerMixin());
    return res.data;
}

export function createWebSocketUri(gameHistoryId: number): string {
    return `ws://localhost:8080/chess-app-gamestate/api/gamestate/${gameHistoryId}?bearer=${store.state.token}`;
}
