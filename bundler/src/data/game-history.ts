import axios from 'axios';
import store from '../store';
import { GAMEHISTORY_URI } from '../env-config';
import {
    GameHistoryDto,
    DirectGameRequestDto,
    StatsContainerDto
} from '../types/game-history-types';

const gameHistoryApi = `${GAMEHISTORY_URI}/api/gamehistory`;
const gameRequestApi = `${GAMEHISTORY_URI}/api/gamerequest`;
const statsApi = `${GAMEHISTORY_URI}/api/stats`;

const headers = { 'Content-Type': 'application/json' };

export async function getGameHistory(
    gameHistoryId: number
): Promise<GameHistoryDto> {
    const result = await axios.get(
        `${gameHistoryApi}/${gameHistoryId}`,
        store.getters.bearerMixin()
    );
    return result.data as GameHistoryDto;
}

export async function getGames(): Promise<GameHistoryDto[]> {
    const result = await axios.get(gameHistoryApi, store.getters.bearerMixin());
    return result.data;
}

export async function getCompletedGames(
    userId?: number
): Promise<GameHistoryDto[]> {
    let uri = `${gameHistoryApi}?isCompleted=true`;
    if (userId !== undefined) {
        uri += `&userId=${userId}`;
    }
    const res = await axios.get(uri, store.getters.bearerMixin());
    return res.data;
}

export async function getOutgoingDirectRequests(): Promise<
    DirectGameRequestDto[]
> {
    const result = await axios.get(
        `${gameRequestApi}/direct/outgoing`,
        store.getters.bearerMixin()
    );
    return result.data;
}

export async function getIncomingDirectRequests(): Promise<
    DirectGameRequestDto[]
> {
    const result = await axios.get(
        `${gameRequestApi}/direct/incoming`,
        store.getters.bearerMixin()
    );
    return result.data;
}

export async function requestGameDirect(requesteeId: number): Promise<void> {
    await axios.post(
        `${gameRequestApi}/direct?to=${requesteeId}`,
        null,
        store.getters.bearerMixin({ headers })
    );
}

export async function declineRequest(requestId: number): Promise<void> {
    await axios.post(
        `${gameRequestApi}/direct/${requestId}/decline`,
        null,
        store.getters.bearerMixin({ headers })
    );
}

export async function acceptRequest(
    requestId: number
): Promise<GameHistoryDto> {
    const result = await axios.post(
        `${gameRequestApi}/direct/${requestId}/accept`,
        null,
        store.getters.bearerMixin({ headers })
    );
    return result.data;
}

export async function cancelDirectRequest(
    directRequestId: number
): Promise<void> {
    await axios.delete(
        `${gameRequestApi}/direct/${directRequestId}`,
        store.getters.bearerMixin()
    );
}

export async function requestGameIndirect(): Promise<void> {
    await axios.post(
        `${gameRequestApi}/indirect`,
        null,
        store.getters.bearerMixin({ headers })
    );
}

export async function getIndirectRequestsFrom(): Promise<any> {
    const result = await axios.get(
        `${gameRequestApi}/indirect`,
        store.getters.bearerMixin()
    );
    return result.data;
}

export async function cancelIndirectRequest(
    indirectRequestId: number
): Promise<void> {
    await axios.delete(
        `${gameRequestApi}/indirect/${indirectRequestId}`,
        store.getters.bearerMixin()
    );
}

export async function getUserStatistics(
    userId: number
): Promise<StatsContainerDto> {
    const result = await axios.get(
        `${statsApi}/user/${userId}`,
        store.getters.bearerMixin()
    );
    return result.data;
}

export async function getVersusStatistics(
    opponentUserId: number
): Promise<StatsContainerDto> {
    const result = await axios.get(
        `${statsApi}/versus/${opponentUserId}`,
        store.getters.bearerMixin()
    );
    return result.data;
}
