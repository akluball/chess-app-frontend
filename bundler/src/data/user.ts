import axios, { AxiosResponse } from 'axios';
import store from '../store';
import { USER_URI } from '../env-config';
import { UserDto } from '../types/user-types';
import { is2xx } from '../utils';

const USER_API = `${USER_URI}/api/user`;
const LOGIN_API = `${USER_URI}/api/login`;
const TOKEN_API = `${USER_URI}/api/token`;
const LOGOUT_API = `${USER_URI}/api/logout`;

function alwaysValid(status: number): boolean {
    return true;
}

const headers = { 'Content-Type': 'application/json' };

export async function login(
    handle: String,
    password: string
): Promise<boolean> {
    const res = await axios.post(`${LOGIN_API}/${handle}`, password, {
        headers,
        validateStatus: alwaysValid
    });
    return is2xx(res);
}

export async function getToken(): Promise<AxiosResponse> {
    return await axios.get(TOKEN_API, { validateStatus: alwaysValid });
}

export async function logout(): Promise<void> {
    await axios.post(LOGOUT_API);
}

export async function getCurrentUser(): Promise<UserDto> {
    const res = await axios.get(USER_API, store.getters.bearerMixin());
    return res.data;
}

export async function getUserById(userId: number): Promise<UserDto> {
    const res = await axios.get(
        `${USER_API}/${userId}`,
        store.getters.bearerMixin()
    );
    return res.data;
}

export async function searchUser(searchString: string): Promise<UserDto[]> {
    const res = await axios.get(
        `${USER_API}/search?searchString=${searchString}`,
        store.getters.bearerMixin()
    );
    return res.data;
}
