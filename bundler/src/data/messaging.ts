import axios from 'axios';
import store from '../store';
import { MESSAGING_URI } from '../env-config';
import { ConversationDto, GameConversationDto } from '../types/messaging-types';

const conversationApi = `${MESSAGING_URI}/api/conversation`;
const gameConversationApi = `${MESSAGING_URI}/api/gameconversation`;

const headers = { 'Content-Type': 'application/json' };

export async function createConversation(
    recipientUserId: number,
    messageText: string
): Promise<ConversationDto> {
    const initializer = {
        senderUserId: store.getters.currentUserId,
        recipientUserIds: [recipientUserId],
        messageText
    };
    const result = await axios.post(
        conversationApi,
        initializer,
        store.getters.bearerMixin({ headers })
    );
    return result.data;
}

export async function getConversationByUserIds(
    userIds: number[]
): Promise<ConversationDto> {
    userIds.push(store.getters.currentUserId);
    const queryString = userIds.map(userId => `userId=${userId}`).join('&');
    const result = await axios.get(
        `${conversationApi}/between?${queryString}`,
        store.getters.bearerMixin()
    );
    return result.data;
}

export async function getConversations(): Promise<ConversationDto[]> {
    const result = await axios.get(
        `${conversationApi}`,
        store.getters.bearerMixin()
    );
    return result.data;
}

export async function getConversation(
    conversationId: number
): Promise<ConversationDto> {
    const result = await axios.get(
        `${conversationApi}/${conversationId}`,
        store.getters.bearerMixin()
    );
    return result.data;
}

export function createConversationWebSocketUri(conversationId: number): string {
    return `ws://localhost:8080/chess-app-messaging/api/conversation/${conversationId}?bearer=${store.state.token}`;
}

export async function getGameConversation(
    gameHistoryId: number
): Promise<GameConversationDto> {
    const result = await axios.get(
        `${gameConversationApi}/${gameHistoryId}`,
        store.getters.bearerMixin()
    );
    return result.data;
}

export function createGameConversationWebSocketUri(gameId: number): string {
    return `ws://localhost:8080/chess-app-messaging/api/gameconversation/${gameId}?bearer=${store.state.token}`;
}
