import Vue from 'vue';
import VueRouter, { RouteConfig, Route } from 'vue-router';
import store from './store';

import Home from './components/Home.vue';
import Login from './components/Login.vue';
import Games from './components/games/Games.vue';
import MyMoveGames from './components/games/MyMoveGames.vue';
import InProgressGames from './components/games/InProgressGames.vue';
import CompletedGames from './components/games/CompletedGames.vue';
import Requests from './components/games/Requests.vue';
import Game from './components/game/Game.vue';
import Conversations from './components/conversations/Conversations.vue';
import Conversation from './components/conversations/Conversation.vue';
import NormalConversation from './components/conversations/model/NormalConversation';
import PotentialConversation from './components/conversations/model/PotentialConversation';
import Users from './components/users/Users.vue';
import Profile from './components/user/Profile.vue';
import User from './components/user/User.vue';
import Docs from './components/docs/Docs.vue';
import Doc from './components/docs/Doc.vue';
import EventEmissionConnector from './EventEmissionConnector';
import { FRONTEND_DEPLOYMENTNAME } from './env-config';

Vue.use(VueRouter);

function extractIntParams(
    ...params: string[]
): (route: Route) => { [index: string]: number } {
    return (route: Route) => {
        const extracted: { [index: string]: number } = {};
        params.forEach(param => {
            extracted[param] = parseInt(route.params[param]);
        });
        return extracted;
    };
}

const routes: RouteConfig[] = [
    {
        path: '/docs',
        component: Docs,
        meta: { titlePostfix: 'Docs' },
        children: [{ path: '/docs/:serviceName', component: Doc, props: true }]
    },
    { path: '/login', component: Login, meta: { titlePostfix: 'Login' } },
    { path: '', redirect: '/home' },
    {
        path: '/home',
        component: Home,
        redirect: '/home/games',
        meta: { titlePostfix: 'Home' },
        children: [
            {
                path: 'games',
                component: Games,
                redirect: 'games/mymove',
                children: [
                    { path: 'mymove', component: MyMoveGames },
                    { path: 'inprogress', component: InProgressGames },
                    { path: 'completed', component: CompletedGames },
                    { path: 'requests', component: Requests }
                ]
            },
            {
                path: 'game/:gameHistoryId',
                component: Game,
                props: extractIntParams('gameHistoryId')
            },
            {
                path: 'conversations',
                component: Conversations,
                children: [
                    {
                        path: 'conversation/:conversationId',
                        component: Conversation,
                        props: route => {
                            const eventEmissionConnector = new EventEmissionConnector();
                            const conversation = new NormalConversation(
                                parseInt(route.params.conversationId),
                                eventEmissionConnector
                            );
                            return { conversation, eventEmissionConnector };
                        }
                    },
                    {
                        path: 'conversation/with/:userId',
                        component: Conversation,
                        props: route => {
                            const eventEmissionConnector = new EventEmissionConnector();
                            const conversation = new PotentialConversation(
                                parseInt(route.params.userId),
                                eventEmissionConnector
                            );
                            return { conversation, eventEmissionConnector };
                        },
                        beforeEnter(to, from, next) {
                            const userId = parseInt(to.params.userId);
                            if (userId === store.getters.currentUserId) {
                                next('/home/conversations');
                            } else {
                                next();
                            }
                        }
                    }
                ]
            },
            { path: 'profile', component: Profile },
            { path: 'users', component: Users },
            {
                path: 'user/:userId',
                component: User,
                props: extractIntParams('userId'),
                beforeEnter(to, from, next) {
                    const userId = parseInt(to.params.userId);
                    if (userId === store.getters.currentUserId) {
                        next('/home/profile');
                    } else {
                        next();
                    }
                },
                redirect: 'user/:userId/inprogress',
                children: [
                    {
                        path: 'inprogress',
                        component: InProgressGames,
                        props: extractIntParams('userId')
                    },
                    {
                        path: 'completed',
                        component: CompletedGames,
                        props: extractIntParams('userId')
                    }
                ]
            }
        ]
    },
    { path: '*', redirect: '/home' }
];

const router = new VueRouter({
    base: `/${FRONTEND_DEPLOYMENTNAME}`,
    mode: 'history',
    routes
});

const noAuthWhitelist = [/^\/login(?:\/.*)?/, /^\/docs(?:\/.*)?/];

// check if page requires user to be logged in
router.beforeEach(async (to: Route, from: Route, next: Function) => {
    const doesNotRequireLogin =
        noAuthWhitelist.findIndex(routeRegex => routeRegex.test(to.fullPath)) >
        -1;
    if (doesNotRequireLogin) {
        next();
    } else {
        if (store.getters.hasToken) {
            next();
        } else {
            await store.dispatch('getToken');
            if (store.getters.hasToken) {
                await store.dispatch('getCurrentUser');
                next();
            } else {
                next('/login');
            }
        }
    }
});

// add title postfix if present
router.beforeEach((to: Route, from: Route, next: Function) => {
    const routeWithTitlePostfix = to.matched
        .slice()
        .reverse()
        .find(route => !!route.meta.titlePostfix);
    const titlePostfix =
        routeWithTitlePostfix === undefined
            ? ''
            : ` - ${routeWithTitlePostfix.meta.titlePostfix}`;
    document.title = `Chess Application${titlePostfix}`;
    next();
});

export default router;
