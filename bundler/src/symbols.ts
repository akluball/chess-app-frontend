import { unicode2Html } from './utils';

const pieceLetters = 'KQRBNP';

export function pieceIconSymbolFor(pieceLetter: string) {
    return unicode2Html(9818 + pieceLetters.indexOf(pieceLetter));
}

export const SEND_SYMBOL = unicode2Html(10148);
export const CHECK_MARK = unicode2Html(10004);
export const CROSS_MARK = unicode2Html(10008);
export const SLASH_THRU_CIRCLE = unicode2Html(9033);
export const PREVIOUS_ARROW = unicode2Html(10094);
export const NEXT_ARROW = unicode2Html(10095);
