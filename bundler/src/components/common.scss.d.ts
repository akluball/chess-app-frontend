interface StyleVariables {
    primaryDarkColor: string;
    primaryLightColor: string;
    winSliceColor: string;
    lossSliceColor: string;
    drawSliceColor: string;
    emptyPieColor: string;
}

export const styleVariables: StyleVariables;

export default styleVariables;
