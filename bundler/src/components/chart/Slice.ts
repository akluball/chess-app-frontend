export default interface Slice {
    key: string;
    color: string;
    count: number;
}
