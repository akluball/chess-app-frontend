import MoveSummary from './MoveSummary';
import { isEven } from '../../../utils';
import { MoveResultDto, MoveSummaryDto } from '../../../types/game-state-types';
import Board from './Board';
import PassiveBoard from './PassiveBoard';

export default class MoveHistory {
    private _moveSummaries: MoveSummary[] = [];
    private boards: PassiveBoard[] = [];

    constructor() {
        const initialBoard = new PassiveBoard.Builder()
            .place('a1', 'wR')
            .place('b1', 'wN')
            .place('c1', 'wB')
            .place('d1', 'wQ')
            .place('e1', 'wK')
            .place('f1', 'wB')
            .place('g1', 'wN')
            .place('h1', 'wR')
            .place('a2', 'wP')
            .place('b2', 'wP')
            .place('c2', 'wP')
            .place('d2', 'wP')
            .place('e2', 'wP')
            .place('f2', 'wP')
            .place('g2', 'wP')
            .place('h2', 'wP')
            .place('a7', 'bP')
            .place('b7', 'bP')
            .place('c7', 'bP')
            .place('d7', 'bP')
            .place('e7', 'bP')
            .place('f7', 'bP')
            .place('g7', 'bP')
            .place('h7', 'bP')
            .place('a8', 'bR')
            .place('b8', 'bN')
            .place('c8', 'bB')
            .place('d8', 'bQ')
            .place('e8', 'bK')
            .place('f8', 'bB')
            .place('g8', 'bN')
            .place('h8', 'bR')
            .build();
        this.boards.push(initialBoard);
    }

    get moveSummaries(): MoveSummary[] {
        return this._moveSummaries;
    }

    isWhiteSideTurn(): boolean {
        return isEven(this._moveSummaries.length);
    }

    isMostRecentMove(moveNumber: number): boolean {
        return moveNumber === this._moveSummaries.length;
    }

    addFromMoveResultDto(moveResultDto: MoveResultDto): void {
        this._moveSummaries.push(
            new MoveSummary(
                this._moveSummaries.length + 1,
                moveResultDto.moveSummary
            )
        );
    }

    addFromMoveSummaryDto(moveSummaryDto: MoveSummaryDto): void {
        this._moveSummaries.push(
            new MoveSummary(this._moveSummaries.length + 1, moveSummaryDto)
        );
    }

    get whiteSideTotalMillis(): number {
        return this._moveSummaries
            .filter(({}, index) => isEven(index))
            .reduce(
                (totalMillis, moveSummary) =>
                    totalMillis + moveSummary.durationMillis,
                0
            );
    }

    get blackSideTotalMillis(): number {
        return this._moveSummaries
            .filter(({}, index) => !isEven(index))
            .reduce(
                (totalMillis, moveSummary) =>
                    totalMillis + moveSummary.durationMillis,
                0
            );
    }

    get lastMoveSummary(): MoveSummary {
        return this._moveSummaries[this._moveSummaries.length - 1];
    }

    private computeBoards(moveNumber: number): void {
        while (moveNumber > this.boards.length - 1) {
            const lastBoard = this.boards[this.boards.length - 1];
            const nextMoveSummary = this._moveSummaries[this.boards.length - 1];
            this.boards.push(lastBoard.computeBoardAfterMove(nextMoveSummary));
        }
    }

    getBoardAfterMove(moveNumber: number): Board {
        if (moveNumber > this.boards.length - 1) {
            this.computeBoards(moveNumber);
        }
        return this.boards[moveNumber];
    }

    get lastBoard(): Board {
        return this.getBoardAfterMove(this._moveSummaries.length);
    }
}
