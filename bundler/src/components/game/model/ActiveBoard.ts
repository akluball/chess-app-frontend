import Board from './Board';
import Piece, { MovedPiece, PieceCapture } from './Piece';
import { createWebSocketUri, getGameState } from '../../../data/game-state';
import {
    MoveDto,
    MoveResultDto,
    GameStateDto
} from '../../../types/game-state-types';
import Position from './Position';
import MoveSummary from './MoveSummary';
import Game from './Game';

class GameStateWebSocket {
    private activeBoard: ActiveBoard;
    private webSocket: WebSocket;

    constructor(activeBoard: ActiveBoard, gameHistoryId: number) {
        this.activeBoard = activeBoard;
        this.webSocket = new WebSocket(createWebSocketUri(gameHistoryId));
        this.webSocket.onopen = this.onOpen.bind(this);
        this.webSocket.onclose = this.onClosed.bind(this);
        this.webSocket.onerror = this.onError.bind(this);
        this.webSocket.onmessage = this.onMessage.bind(this);
    }

    onOpen(event: Event) {
        console.log('gamestate websocket open');
    }

    onClosed(event: CloseEvent) {
        console.log('gamestate websocket closed');
    }

    onError(event: Event) {
        console.log('gamestate websocket error');
        console.error(event);
    }

    onMessage(messageEvent: MessageEvent) {
        this.activeBoard.handleMoveResult(JSON.parse(messageEvent.data));
    }

    attemptMove(moveDto: MoveDto) {
        this.webSocket.send(JSON.stringify(moveDto));
    }

    close() {
        this.webSocket.close();
    }
}

export default class ActiveBoard implements Board {
    private _pieces: Piece[] = [];
    private game: Game;
    private gameStateWebSocket: GameStateWebSocket;
    private movedPiece: MovedPiece | undefined;
    private pieceCapture: PieceCapture | undefined;

    constructor(game: Game) {
        this.game = game;
        this.gameStateWebSocket = new GameStateWebSocket(
            this,
            this.game.gameHistoryId
        );
        getGameState(this.game.gameHistoryId)
            .then(gameStateDto => {
                gameStateDto.moveSummaries.forEach(
                    this.game.moveHistory.addFromMoveSummaryDto.bind(
                        this.game.moveHistory
                    )
                );
                this.syncTimers(gameStateDto);
                for (let [positionAsString, pieceAsString] of Object.entries(
                    gameStateDto.positionToPiece
                )) {
                    this._pieces.push(
                        Piece.create(positionAsString, pieceAsString)
                    );
                }
            })
            .catch(this.deactivate.bind(this));
    }

    get pieces(): Piece[] {
        return this._pieces;
    }

    private onTimerExpiration(): void {
        getGameState(this.game.gameHistoryId).catch(this.deactivate.bind(this));
    }

    private syncTimers(timeRemainingAwareDto: MoveResultDto | GameStateDto) {
        this.game.whiteSideTimer.millisRemaining =
            timeRemainingAwareDto.whiteSideMillisRemaining;
        this.game.blackSideTimer.millisRemaining =
            timeRemainingAwareDto.blackSideMillisRemaining;
        if (this.game.moveHistory.isWhiteSideTurn()) {
            this.game.whiteSideTimer.stop();
            this.game.blackSideTimer.stop();
            this.game.whiteSideTimer.start();
            this.game.whiteSideTimer.onExpiration(
                this.onTimerExpiration.bind(this)
            );
        } else {
            this.game.whiteSideTimer.stop();
            this.game.blackSideTimer.stop();
            this.game.blackSideTimer.start();
            this.game.blackSideTimer.onExpiration(
                this.onTimerExpiration.bind(this)
            );
        }
    }

    private sync() {
        getGameState(this.game.gameHistoryId)
            .then(gameStateDto => {
                this.syncTimers(gameStateDto);
                const updated: Piece[] = [];
                for (let [positionAsString, pieceAsString] of Object.entries(
                    gameStateDto.positionToPiece
                )) {
                    let found = this.pieces.find(piece => {
                        return piece.isStringReprMatch(
                            positionAsString,
                            pieceAsString
                        );
                    });
                    if (found === undefined) {
                        updated.push(
                            Piece.create(positionAsString, pieceAsString)
                        );
                    } else {
                        updated.push(found);
                    }
                }
                this._pieces.splice(0, this._pieces.length, ...updated);
            })
            .catch(this.deactivate.bind(this));
    }

    private deactivate(): void {
        this.gameStateWebSocket.close();
        this.game.deactivate();
    }

    attemptMove(
        piece: Piece,
        targetFileAsInt: number,
        targetRankAsInt: number
    ) {
        const target = Position.getPositionFromIntegerCoordinates(
            targetFileAsInt,
            targetRankAsInt
        );
        this.gameStateWebSocket.attemptMove({
            pieceLetter: piece.asLetter,
            promotionPieceLetter: 'Q',
            source: piece.position.toMovetextRepr(),
            target: target.toMovetextRepr()
        });
        this.pieceCapture = new PieceCapture(this, target);
        this.movedPiece = new MovedPiece(piece, target);
    }

    private executeMoveSummary(moveSummary: MoveSummary) {
        const movingPiece = this._pieces.find(
            moveSummary.isPieceToMove.bind(moveSummary)
        );
        if (movingPiece === undefined) {
            this.sync();
        } else {
            this.pieceCapture = new PieceCapture(this, moveSummary.target);
            this.movedPiece = new MovedPiece(movingPiece, moveSummary.target);
        }
    }

    handleMoveResult(moveResultDto: MoveResultDto) {
        if (!moveResultDto.wasExecuted) {
            if (this.movedPiece !== undefined) {
                this.movedPiece.undo();
                if (this.pieceCapture !== undefined) {
                    this.pieceCapture.undo();
                }
            }
        } else {
            this.game.moveHistory.addFromMoveResultDto(moveResultDto);
            const moveSummary = this.game.moveHistory.lastMoveSummary;
            this.syncTimers(moveResultDto);
            if (this.movedPiece !== undefined) {
                if (!this.movedPiece.matchesMoveSummary(moveSummary)) {
                    this.movedPiece.undo();
                    if (this.pieceCapture !== undefined) {
                        this.pieceCapture.undo();
                    }
                    this.executeMoveSummary(moveSummary);
                }
            } else {
                this.executeMoveSummary(moveSummary);
            }
            if (moveSummary.isPawnPromotion()) {
                this._pieces.splice(
                    this._pieces.findIndex(
                        moveSummary.isAtTarget.bind(moveSummary)
                    ),
                    1
                );
                this._pieces.push(moveSummary.pawnPromotedPiece);
            }
            if (
                moveSummary.isPawnCapture() &&
                this.pieceCapture !== undefined &&
                !this.pieceCapture.isCapture()
            ) {
                const enPassantCaptureIndex = this._pieces.findIndex(
                    moveSummary.isAtEnPassantCapturedPosition.bind(moveSummary)
                );
                this._pieces.splice(enPassantCaptureIndex, 1);
            }
            if (moveSummary.isCastle()) {
                const castlingRookPosition = moveSummary.castlingRookSource;
                const castlingRookIndex = this._pieces.findIndex(piece =>
                    piece.position.equals(castlingRookPosition)
                );
                if (castlingRookIndex === -1) {
                    this.sync();
                }
                const castlingRookTarget = moveSummary.castlingRookTarget;
                if (castlingRookTarget !== undefined) {
                    this._pieces[castlingRookIndex].moveTo(castlingRookTarget);
                } else {
                    this.sync();
                }
            }
            if (moveSummary.isEndedGame()) {
                this.deactivate();
            }
        }
        this.movedPiece = undefined;
        this.pieceCapture = undefined;
    }
}
