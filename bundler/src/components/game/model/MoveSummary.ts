import { MoveSummaryDto } from '../../../types/game-state-types';
import { toTimerString, isEven } from '../../../utils';
import Piece from './Piece';
import Position from './Position';
import { SourceDisambiguation } from '../../../types/game-history-types';

function isCastleLeft(moveSummaryDto: MoveSummaryDto): boolean {
    return (
        moveSummaryDto.pieceLetter === 'K' &&
        ((moveSummaryDto.source === 'e1' && moveSummaryDto.target === 'c1') ||
            (moveSummaryDto.source === 'e8' && moveSummaryDto.target === 'c8'))
    );
}

function isCastleRight(moveSummaryDto: MoveSummaryDto): boolean {
    return (
        moveSummaryDto.pieceLetter === 'K' &&
        ((moveSummaryDto.source === 'e1' && moveSummaryDto.target === 'g1') ||
            (moveSummaryDto.source === 'e8' && moveSummaryDto.target === 'g8'))
    );
}

function computeMovetext(moveSummaryDto: MoveSummaryDto): string {
    let movetext = '';
    if (isCastleLeft(moveSummaryDto)) {
        movetext += 'O-O-O';
    } else if (isCastleRight(moveSummaryDto)) {
        movetext += 'O-O';
    } else {
        if (moveSummaryDto.pieceLetter !== 'P') {
            movetext += moveSummaryDto.pieceLetter;
        }
        switch (moveSummaryDto.sourceDisambiguation) {
            case SourceDisambiguation.FILE:
                movetext += moveSummaryDto.source.charAt(0);
                break;
            case SourceDisambiguation.RANK:
                movetext += moveSummaryDto.source.charAt(1);
                break;
            case SourceDisambiguation.BOTH:
                movetext += moveSummaryDto.source;
                break;
        }
        if (moveSummaryDto.isCapture) {
            movetext += 'x';
        }
        movetext += moveSummaryDto.target;
        if (moveSummaryDto.promotionPieceLetter) {
            movetext += `=${moveSummaryDto.promotionPieceLetter}`;
        }
    }
    if (moveSummaryDto.gameStatus.endsWith('check')) {
        movetext += '+';
    } else if (moveSummaryDto.gameStatus.endsWith('checkmate')) {
        movetext += '#';
    }
    return movetext;
}

export default class MoveSummary {
    private _moveNumber: number;
    private _movetext: string;
    private _duration: string;
    private _gameStatus: string;
    private moveSummaryDto: MoveSummaryDto;
    private source: Position;
    private _target: Position;
    private pieceLetter: string;
    private promotionPieceLetter: string;

    constructor(moveNumber: number, moveSummaryDto: MoveSummaryDto) {
        this._moveNumber = moveNumber;
        this._movetext = computeMovetext(moveSummaryDto);
        this._duration = toTimerString(moveSummaryDto.durationMillis);
        this._gameStatus = moveSummaryDto.gameStatus.replace(/-/g, ' ');
        this.moveSummaryDto = moveSummaryDto;
        this.source = Position.getPositionFromString(moveSummaryDto.source);
        this._target = Position.getPositionFromString(moveSummaryDto.target);
        this.pieceLetter = moveSummaryDto.pieceLetter;
        this.promotionPieceLetter = moveSummaryDto.promotionPieceLetter;
    }

    get moveNumber(): number {
        return this._moveNumber;
    }

    get movetext(): string {
        return this._movetext;
    }

    get duration(): string {
        return this._duration;
    }

    get gameStatus(): string {
        return this._gameStatus;
    }

    get target(): Position {
        return this._target;
    }

    get durationMillis(): number {
        return this.moveSummaryDto.durationMillis;
    }

    moveToTarget(piece: Piece): void {
        piece.moveTo(this._target);
    }

    isWhiteSideMove(): boolean {
        return !isEven(this._moveNumber);
    }

    isCapture(): boolean {
        return this.moveSummaryDto.isCapture;
    }

    private getPieceAsString(): string {
        return `${this.isWhiteSideMove() ? 'w' : 'b'}${this.pieceLetter}`;
    }

    isPieceToMove(pieceToCheck: Piece): boolean {
        return (
            pieceToCheck.asLetter === this.pieceLetter &&
            pieceToCheck.isWhite === this.isWhiteSideMove() &&
            pieceToCheck.position.equals(this.source)
        );
    }

    isPieceAfterMove(pieceToCheck: Piece): boolean {
        return (
            this._target.equals(pieceToCheck.position) &&
            this.pieceLetter === pieceToCheck.asLetter
        );
    }

    isAtTarget(pieceToCheck: Piece): boolean {
        return this._target.equals(pieceToCheck.position);
    }

    isPawnCapture(): boolean {
        return (
            this.pieceLetter === 'P' &&
            this.source.fileAsInt !== this._target.fileAsInt
        );
    }

    isAtEnPassantCapturedPosition(pieceToCheck: Piece): boolean {
        const enPassantCapturePosition = Position.getPositionFromIntegerCoordinates(
            this._target.fileAsInt,
            this.source.rankAsInt
        );
        return enPassantCapturePosition.equals(pieceToCheck.position);
    }

    isPawnPromotion(): boolean {
        return (
            this.pieceLetter === 'P' &&
            this.promotionPieceLetter !== undefined &&
            this.promotionPieceLetter.length !== 0
        );
    }

    get pawnPromotedPiece(): Piece {
        const promotedPieceAsString = `${this.isWhiteSideMove() ? 'w' : 'b'}${
            this.promotionPieceLetter
        }`;
        return Piece.create(this._target, promotedPieceAsString);
    }

    isCastle(): boolean {
        switch (this.getPieceAsString()) {
            case 'wK':
                return (
                    this.source.isMovetextRepr('e1') &&
                    (this._target.isMovetextRepr('c1') ||
                        this._target.isMovetextRepr('g1'))
                );
            case 'bK':
                return (
                    this.source.isMovetextRepr('e8') &&
                    (this._target.isMovetextRepr('c8') ||
                        this._target.isMovetextRepr('g8'))
                );
            default:
                return false;
        }
    }

    get castlingRookSource(): Position | undefined {
        switch (this.getPieceAsString()) {
            case 'wK':
                switch (this._target.toMovetextRepr()) {
                    case 'c1':
                        return Position.getPositionFromString('a1');
                    case 'g1':
                        return Position.getPositionFromString('h1');
                    default:
                        return undefined;
                }
            case 'bK':
                switch (this._target.toMovetextRepr()) {
                    case 'c8':
                        return Position.getPositionFromString('a8');
                    case 'g8':
                        return Position.getPositionFromString('h8');
                    default:
                        return undefined;
                }

            default:
                return undefined;
        }
    }

    get castlingRookTarget(): Position | undefined {
        switch (this.getPieceAsString()) {
            case 'wK':
                switch (this._target.toMovetextRepr()) {
                    case 'c1':
                        return Position.getPositionFromString('d1');
                    case 'g1':
                        return Position.getPositionFromString('f1');
                    default:
                        return undefined;
                }
            case 'bK':
                switch (this._target.toMovetextRepr()) {
                    case 'c8':
                        return Position.getPositionFromString('d8');
                    case 'g8':
                        return Position.getPositionFromString('f8');
                    default:
                        return undefined;
                }

            default:
                return undefined;
        }
    }

    isEndedGame(): boolean {
        return (
            ['in-progress', 'white-in-check', 'black-in-check'].indexOf(
                this.moveSummaryDto.gameStatus
            ) === -1
        );
    }
}
