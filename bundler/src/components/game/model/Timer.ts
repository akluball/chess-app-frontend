import { toTimerString } from '../../../utils';

export default class Timer {
    private timerUpdateIntervalId: number | undefined;
    private onExpirationTimeoutId: number | undefined;
    private millisPerPlayer: number;
    private _millisRemaining: number;
    private _timeRemainingRatio: number;
    private _timerString: string;
    private _isRunning: boolean;
    private onExpirationCallbacks: Function[] = [];

    constructor(millisPerPlayer: number) {
        this.millisPerPlayer = millisPerPlayer;
        this._millisRemaining = 0;
        this._timeRemainingRatio = 0;
        this._timerString = '';
        this._isRunning = false;
    }

    get millisRemaining(): number {
        return this._millisRemaining;
    }

    set millisRemaining(millisRemaining: number) {
        this._millisRemaining = millisRemaining;
        this.updateTimeRemainingRatio();
        this.updateTimerString();
    }

    get timeRemainingRatio(): number {
        return this._timeRemainingRatio;
    }

    get timerString(): string {
        return this._timerString;
    }

    get isRunning(): boolean {
        return this._isRunning;
    }

    private updateTimeRemainingRatio() {
        this._timeRemainingRatio = this._millisRemaining / this.millisPerPlayer;
    }

    private updateTimerString() {
        this._timerString = toTimerString(this._millisRemaining);
    }

    start() {
        this._isRunning = true;
        let start = new Date();
        this.onExpirationTimeoutId = window.setTimeout(() => {
            this.onExpirationCallbacks.forEach(callback => callback());
        }, this._millisRemaining + 500);
        this.timerUpdateIntervalId = window.setInterval(() => {
            const now = new Date();
            const elapsedMillis = now.getTime() - start.getTime();
            start = now;
            this._millisRemaining -= elapsedMillis;
            this.updateTimeRemainingRatio();
            this.updateTimerString();
        }, 200);
    }

    stop() {
        this._isRunning = false;
        this.onExpirationCallbacks.splice(0, this.onExpirationCallbacks.length);
        window.clearInterval(this.timerUpdateIntervalId);
        window.clearTimeout(this.onExpirationTimeoutId);
    }

    onExpiration(callback: Function): void {
        this.onExpirationCallbacks.push(callback);
    }
}
