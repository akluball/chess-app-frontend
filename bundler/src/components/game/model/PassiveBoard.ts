import Board from './Board';
import Piece from './Piece';
import MoveSummary from './MoveSummary';

export default class PassiveBoard implements Board {
    private _pieces: Piece[] = [];

    private constructor() {}

    get pieces(): Piece[] {
        return this._pieces;
    }

    attemptMove(
        piece: Piece,
        targetFileAsInt: number,
        targetRankAsInt: number
    ): void {}

    private invalidMoveAttempt(moveSummary: MoveSummary): void {
        console.error('invalid move attempt');
        console.log(this._pieces);
        console.log(moveSummary);
        throw new Error('invalid move attempt');
    }

    computeBoardAfterMove(moveSummary: MoveSummary): PassiveBoard {
        const piecesAfterMove = this._pieces.map(piece => piece.clone());
        const movedPieceIndex = piecesAfterMove.findIndex(
            moveSummary.isPieceToMove.bind(moveSummary)
        );
        if (movedPieceIndex === -1) {
            this.invalidMoveAttempt(moveSummary);
        }
        const movedPiece = piecesAfterMove[movedPieceIndex];
        piecesAfterMove.splice(movedPieceIndex, 1);
        if (moveSummary.isCapture()) {
            let capturedPieceIndex = piecesAfterMove.findIndex(
                moveSummary.isAtTarget.bind(moveSummary)
            );
            if (capturedPieceIndex === -1) {
                if (moveSummary.isPawnCapture()) {
                    capturedPieceIndex = piecesAfterMove.findIndex(
                        moveSummary.isAtEnPassantCapturedPosition.bind(
                            moveSummary
                        )
                    );
                }
                if (capturedPieceIndex === -1) {
                    this.invalidMoveAttempt(moveSummary);
                }
            }
            piecesAfterMove.splice(capturedPieceIndex, 1);
        }
        if (moveSummary.isPawnPromotion()) {
            piecesAfterMove.push(moveSummary.pawnPromotedPiece);
        } else {
            moveSummary.moveToTarget(movedPiece);
            piecesAfterMove.push(movedPiece);
        }
        if (moveSummary.isCastle()) {
            const castlingRookPosition = moveSummary.castlingRookSource;
            const castlingRookIndex = piecesAfterMove.findIndex(piece =>
                piece.position.equals(castlingRookPosition)
            );
            if (castlingRookIndex === -1) {
                this.invalidMoveAttempt(moveSummary);
            }
            const castlingRookTarget = moveSummary.castlingRookTarget;
            if (castlingRookTarget !== undefined) {
                piecesAfterMove[castlingRookIndex].moveTo(castlingRookTarget);
            } else {
                this.invalidMoveAttempt(moveSummary);
            }
        }
        const boardAfterMove = new PassiveBoard();
        boardAfterMove._pieces.splice(0, 0, ...piecesAfterMove);
        return boardAfterMove;
    }

    static Builder = class Builder {
        private passiveBoard: PassiveBoard;

        constructor() {
            this.passiveBoard = new PassiveBoard();
        }

        place(positionAsString: string, pieceAsString: string): Builder {
            this.passiveBoard._pieces.push(
                Piece.create(positionAsString, pieceAsString)
            );
            return this;
        }

        build(): PassiveBoard {
            return this.passiveBoard;
        }
    };
}
