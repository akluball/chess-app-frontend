import Position from './Position';
import { pieceIconSymbolFor } from '../../../symbols';
import Board from './Board';
import MoveSummary from './MoveSummary';

let id = 1;

export default class Piece {
    private _id: number;
    private _position: Position;
    private _isWhite: boolean;
    private _asLetter: string;
    private _icon: string;

    private constructor(
        position: Position,
        isWhite: boolean,
        asLetter: string,
        icon: string
    ) {
        this._id = id++;
        this._position = position;
        this._isWhite = isWhite;
        this._asLetter = asLetter;
        this._icon = icon;
    }

    static create(position: Position | string, piece: string): Piece {
        if (typeof position === 'string') {
            position = Position.getPositionFromString(position);
        }
        const pieceAsLetter = piece.charAt(1);
        return new Piece(
            position,
            piece.startsWith('w'),
            pieceAsLetter,
            pieceIconSymbolFor(pieceAsLetter)
        );
    }

    get id(): number {
        return this._id;
    }

    get position(): Position {
        return this._position;
    }

    get isWhite(): boolean {
        return this._isWhite;
    }

    get asLetter(): string {
        return this._asLetter;
    }

    get icon(): string {
        return this._icon;
    }

    moveTo(position: Position): void {
        this._position = position;
    }

    clone(): Piece {
        return new Piece(
            this._position,
            this._isWhite,
            this._asLetter,
            this._icon
        );
    }

    isStringReprMatch(
        positionAsString: string,
        pieceAsString: string
    ): boolean {
        return (
            this._position.isMovetextRepr(positionAsString) &&
            this._isWhite === pieceAsString.startsWith('w') &&
            this._asLetter === pieceAsString.charAt(1)
        );
    }
}

export class MovedPiece {
    private piece: Piece;
    private previousPosition: Position;

    constructor(piece: Piece, target: Position) {
        this.piece = piece;
        this.previousPosition = this.piece.position;
        this.piece.moveTo(target);
    }

    matchesMoveSummary(moveSummary: MoveSummary): boolean {
        return moveSummary.isPieceAfterMove(this.piece);
    }

    undo() {
        this.piece.moveTo(this.previousPosition);
    }
}

export class PieceCapture {
    private board: Board;
    private capturedPiece: Piece | undefined;

    constructor(board: Board, target: Position) {
        this.board = board;
        const captureIndex = this.board.pieces.findIndex(piece =>
            piece.position.equals(target)
        );
        if (captureIndex !== -1) {
            this.capturedPiece = this.board.pieces[captureIndex];
            this.board.pieces.splice(captureIndex, 1);
        }
    }

    isCapture(): boolean {
        return this.capturedPiece !== undefined;
    }

    undo() {
        if (this.isCapture()) {
            if (this.capturedPiece !== undefined) {
                this.board.pieces.push(this.capturedPiece);
            }
        }
    }
}
