import store from '../../../store';
import Timer from './Timer';
import ActiveBoard from './ActiveBoard';
import MoveHistory from './MoveHistory';
import Board, { createBoardPlaceholder } from './Board';
import { getGameHistory } from '../../../data/game-history';
import {
    ResultTag,
    TerminationTag,
    GameHistoryDto
} from '../../../types/game-history-types';

export default class Game {
    private _gameHistoryId: number;
    private _isWhiteSideUser: boolean = false;
    private _whiteSideTimer: Timer;
    private _blackSideTimer: Timer;
    private _moveHistory: MoveHistory;
    private activeBoard: ActiveBoard | undefined;
    private _focusedBoard: Board = createBoardPlaceholder();
    private _outcome: string = '';

    constructor(gameHistoryId: number) {
        this._gameHistoryId = gameHistoryId;
        this._whiteSideTimer = new Timer(0);
        this._blackSideTimer = new Timer(0);
        this._moveHistory = new MoveHistory();
        getGameHistory(this._gameHistoryId)
            .then(gameHistoryDto => {
                this._isWhiteSideUser =
                    store.getters.currentUserId ===
                    gameHistoryDto.whiteSideUserId;
                this._whiteSideTimer = new Timer(
                    gameHistoryDto.millisPerPlayer
                );
                this._blackSideTimer = new Timer(
                    gameHistoryDto.millisPerPlayer
                );
                if (gameHistoryDto.resultTag === ResultTag.IN_PROGRESS) {
                    this._focusedBoard = this.activeBoard = new ActiveBoard(
                        this
                    );
                } else {
                    gameHistoryDto.moveSummaries.forEach(
                        this._moveHistory.addFromMoveSummaryDto.bind(
                            this._moveHistory
                        )
                    );
                    this.updateTimersFromMoveHistory();
                    this._outcome = this.computeOutcome(gameHistoryDto);
                    this._focusedBoard = this._moveHistory.lastBoard;
                }
            })
            .catch(console.error);
    }

    get gameHistoryId(): number {
        return this._gameHistoryId;
    }

    get isWhiteSideUser(): boolean {
        return this._isWhiteSideUser;
    }

    get whiteSideTimer(): Timer {
        return this._whiteSideTimer;
    }

    get blackSideTimer(): Timer {
        return this._blackSideTimer;
    }

    get moveHistory(): MoveHistory {
        return this._moveHistory;
    }

    get focusedBoard(): Board {
        return this._focusedBoard;
    }

    get isTerminated(): boolean {
        return this.activeBoard === undefined && this._outcome.length > 0;
    }

    get outcome(): string {
        return this._outcome;
    }

    private updateTimersFromMoveHistory(): void {
        getGameHistory(this._gameHistoryId)
            .then(gameHistoryDto => {
                if (
                    gameHistoryDto.terminationTag ===
                        TerminationTag.TIME_FORFEIT &&
                    gameHistoryDto.resultTag === ResultTag.BLACK_WINS
                ) {
                    this._whiteSideTimer.millisRemaining = 0;
                } else {
                    const whiteSideMillisElapsed = this._moveHistory
                        .whiteSideTotalMillis;
                    this._whiteSideTimer.millisRemaining =
                        gameHistoryDto.millisPerPlayer - whiteSideMillisElapsed;
                }
                if (
                    gameHistoryDto.terminationTag ===
                        TerminationTag.TIME_FORFEIT &&
                    gameHistoryDto.resultTag === ResultTag.WHITE_WINS
                ) {
                    this._blackSideTimer.millisRemaining = 0;
                } else {
                    const blackSideMillisElapsed = this._moveHistory
                        .blackSideTotalMillis;
                    this._blackSideTimer.millisRemaining =
                        gameHistoryDto.millisPerPlayer - blackSideMillisElapsed;
                }
            })
            .catch(console.error);
    }

    private computeOutcome(gameHistoryDto: GameHistoryDto): string {
        switch (gameHistoryDto.resultTag) {
            case ResultTag.WHITE_WINS:
                return gameHistoryDto.terminationTag === TerminationTag.NORMAL
                    ? 'white side wins by checkmate'
                    : 'white side wins on time';
            case ResultTag.BLACK_WINS:
                return gameHistoryDto.terminationTag === TerminationTag.NORMAL
                    ? 'black side wins by checkmate'
                    : 'black side wins on time';
            case ResultTag.DRAW:
                return 'draw';
            default:
                return '';
        }
    }

    private updateOutcome(): void {
        getGameHistory(this._gameHistoryId)
            .then(gameHistoryDto => {
                this._outcome = this.computeOutcome(gameHistoryDto);
            })
            .catch(console.error);
    }

    focusBoardAfterMove(moveNumber: number): void {
        if (
            this._moveHistory.isMostRecentMove(moveNumber) &&
            this.activeBoard !== undefined
        ) {
            this._focusedBoard = this.activeBoard;
        } else {
            this._focusedBoard = this._moveHistory.getBoardAfterMove(
                moveNumber
            );
        }
    }

    deactivate(): void {
        this._focusedBoard = this._moveHistory.lastBoard;
        this.activeBoard = undefined;
        this._whiteSideTimer.stop();
        this._blackSideTimer.stop();
        this.updateTimersFromMoveHistory();
        this.updateOutcome();
    }
}
