const files = 'abcdefgh';

export default class Position {
    private _fileAsInt: number;
    private _rankAsInt: number;

    private constructor(fileAsInt: number, rankAsInt: number) {
        this._fileAsInt = fileAsInt;
        this._rankAsInt = rankAsInt;
    }

    private static asStringToPosition: { [index: string]: Position } = {};

    static getPositionFromString(positionAsString: string): Position {
        let position: Position | undefined =
            Position.asStringToPosition[positionAsString];
        if (position === undefined) {
            const fileAsInt = files.indexOf(positionAsString.charAt(0)) + 1;
            const rankAsInt = Number.parseInt(positionAsString.charAt(1));
            position = Position.asStringToPosition[
                positionAsString
            ] = new Position(fileAsInt, rankAsInt);
        }
        return position;
    }

    static getPositionFromIntegerCoordinates(
        fileAsInt: number,
        rankAsInt: number
    ) {
        return Position.getPositionFromString(
            `${files.charAt(fileAsInt - 1)}${rankAsInt}`
        );
    }

    get fileAsInt(): number {
        return this._fileAsInt;
    }

    get rankAsInt(): number {
        return this._rankAsInt;
    }

    equals(position: Position | undefined): boolean {
        if (position === undefined) {
            return false;
        } else {
            return this === position;
        }
    }

    isMovetextRepr(movetextRepr: string): boolean {
        return this.toMovetextRepr() === movetextRepr;
    }

    toMovetextRepr(): string {
        return `${files.charAt(this._fileAsInt - 1)}${this._rankAsInt}`;
    }
}
