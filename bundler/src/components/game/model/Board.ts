import Piece from './Piece';

export default interface Board {
    pieces: Piece[];
    attemptMove(
        piece: Piece,
        targetFileAsInt: number,
        targetRankAsInt: number
    ): void;
}

export function createBoardPlaceholder(): Board {
    return {
        pieces: [],
        attemptMove(
            piece: Piece,
            targetFileAsInt: number,
            targetRankAsInt: number
        ): void {}
    };
}
