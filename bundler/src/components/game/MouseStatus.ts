enum MouseStatus {
    Up,
    Down,
    Left
}

export default MouseStatus;
