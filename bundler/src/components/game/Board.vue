<template>
    <div
        ref="container"
        :class="[
            'container',
            useMinimumDimension ? 'container_min-dimension' : ''
        ]"
    >
        <svg
            class="board"
            ref="board"
            :height="boardDimension"
            :width="boardDimension"
            :viewBox="viewBox"
            @mouseup="mouseUp"
            @mousemove="mouseMove"
            @mouseleave="mouseLeave"
        >
            <!-- chess board tiles -->
            <rect
                v-for="(val, i) in 64"
                :class="[
                    (Math.floor(i / 8) + (i % 8)) % 2 === 0
                        ? 'board__tile_dark'
                        : 'board__tile_light'
                ]"
                :key="i"
                :x="computeCoordinate(Math.floor(i / 8))"
                :y="computeCoordinate(computeY(i % 8))"
                :width="tileSize"
                :height="tileSize"
            />
            <!-- file labels -->
            <g v-if="showCoordinates">
                <foreignObject
                    v-for="(file, index) in [
                        'a',
                        'b',
                        'c',
                        'd',
                        'e',
                        'f',
                        'g',
                        'h'
                    ]"
                    :key="`file-${file}`"
                    :x="index * tileSize"
                    :y="7 * tileSize"
                    :height="tileSize"
                    :width="tileSize"
                >
                    <div class="board__file-label">{{ file }}</div>
                </foreignObject>
                <!-- rank labels -->
                <foreignObject
                    v-for="rank in 8"
                    :key="`rank-${rank}`"
                    :x="0"
                    :y="computeY(rank - 1) * tileSize"
                    :height="tileSize"
                    :width="tileSize"
                >
                    <div class="board__rank-label">{{ rank }}</div>
                </foreignObject>
            </g>
            <!-- pieces -->
            <piece
                v-for="piece in boardModel.pieces"
                :board="board"
                :model="piece"
                :key="`piece-${piece.id}`"
                :tileSize="tileSize"
                :svgMouseX="svgMouseX"
                :svgMouseY="svgMouseY"
                :mouseStatus="mouseStatus"
                :isWhiteSideUser="isWhiteSideUser"
                :useLetterIcons="useLetterIcons"
                @select="onSelect"
                @move-attempt="onMoveAttempt"
            />
        </svg>
    </div>
</template>

<script lang="ts">
import Vue from 'vue';
import MouseStatus from './MouseStatus';
import Game from './model/Game';
import PieceComponent from './Piece.vue';
import Piece from './model/Piece';
import ResizeObserver from 'resize-observer-polyfill';
import Board from './model/Board';
export default Vue.extend({
    components: {
        piece: PieceComponent
    },
    props: {
        boardModelInit: Object as () => Board,
        isWhiteSideUser: Boolean,
        useLetterIcons: Boolean,
        showCoordinates: Boolean,
        useMinimumDimension: Boolean
    },
    data() {
        return {
            resizeObserver: undefined as ResizeObserver | undefined,
            container: undefined as HTMLElement | undefined,
            board: undefined as SVGSVGElement | undefined,
            boardRect: undefined as DOMRect | undefined,
            boardModel: this.boardModelInit,
            boardDimension: 0,
            isBeingDragged: false,
            svgMouseX: 0,
            svgMouseY: 0,
            tileSize: 100,
            mouseStatus: MouseStatus.Up
        };
    },
    watch: {
        boardModelInit(updated) {
            this.boardModel = updated;
        }
    },
    mounted() {
        this.container = this.$refs.container as HTMLElement;
        this.board = this.$refs.board as SVGSVGElement;
        this.boardRect = this.board.getBoundingClientRect();
        this.resizeObserver = new ResizeObserver(this.resize);
        this.resizeObserver.observe(this.container);
        this.resize();
    },
    beforeDestroy() {
        if (this.resizeObserver !== undefined) {
            this.resizeObserver.unobserve(this.$refs.container as Element);
        }
    },
    computed: {
        viewBox(): string {
            return `0 0 ${this.tileSize * 8} ${this.tileSize * 8}`;
        }
    },
    methods: {
        resize() {
            if (this.container !== undefined) {
                this.boardDimension = Math.min(
                    this.container.clientWidth,
                    this.container.clientHeight
                );
                this.$nextTick(() => {
                    if (this.board !== undefined) {
                        this.boardRect = this.board.getBoundingClientRect();
                    }
                });
            }
        },
        computeCoordinate(index: number): number {
            return this.tileSize * index;
        },
        onSelect(event: MouseEvent): void {
            this.mapToBoard(event);
            this.mouseStatus = MouseStatus.Down;
        },
        mouseMove(event: MouseEvent): void {
            // only move if a piece is selected
            if (this.mouseStatus === MouseStatus.Down) {
                this.mapToBoard(event);
            }
        },
        mouseUp(event: MouseEvent): void {
            // only move if a piece is selected
            if (this.mouseStatus === MouseStatus.Down) {
                this.mapToBoard(event);
            }
            this.mouseStatus = MouseStatus.Up;
        },
        mouseLeave(): void {
            this.mouseStatus = MouseStatus.Left;
        },
        mapToBoard(event: MouseEvent): void {
            if (this.board !== undefined) {
                if (this.boardRect === undefined) {
                    this.boardRect = this.board.getBoundingClientRect();
                }
                this.svgMouseX =
                    ((event.clientX - this.boardRect.left) *
                        this.board.viewBox.baseVal.width) /
                    this.boardRect.width;
                this.svgMouseY =
                    ((event.clientY - this.boardRect.top) *
                        this.board.viewBox.baseVal.height) /
                    this.boardRect.height;
            }
        },
        computeY(y: number): number {
            return this.isWhiteSideUser ? 7 - y : y;
        },
        onMoveAttempt(
            piece: Piece,
            targetFileAsInt: number,
            targetRankAsInt: number
        ): void {
            this.boardModel.attemptMove(
                piece,
                targetFileAsInt,
                targetRankAsInt
            );
        }
    }
});
</script>

<style scoped lang="scss">
@import '../common.scss';
.container {
    @include centered;
    overflow: auto;
}
.container_min-dimension {
    min-width: $board-min-dimension;
    min-height: $board-min-dimension;
}
.board {
    border: thin solid $board-border-color;
    background-color: $dark-tile-color;
}
.board__tile_dark {
    fill: transparent;
}
.board__tile_light {
    fill: $light-tile-color;
}
.board__file-label {
    width: 100%;
    height: 100%;
    padding-bottom: 0.2em;
    padding-right: 0.2em;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    justify-content: flex-end;
}
.board__rank-label {
    width: 100%;
    height: 100%;
    padding-top: 0.2em;
    padding-left: 0.2em;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
}
</style>
