import Conversation, { Message } from './Conversation';
import { UserDto, createUserDtoPlaceholder } from '../../../types/user-types';
import {
    getConversationByUserIds,
    createConversation
} from '../../../data/messaging';
import router from '../../../router';
import { getUserById } from '../../../data/user';
import EventEmissionConnector from '../../../EventEmissionConnector';

export default class PotentialConversation implements Conversation {
    private _messages: Message[] = [];
    private _otherConverser: UserDto = createUserDtoPlaceholder();
    private eventEmissionConnector: EventEmissionConnector;

    constructor(
        otherConverserId: number,
        eventEmissionConnector: EventEmissionConnector
    ) {
        this.eventEmissionConnector = eventEmissionConnector;
        getConversationByUserIds([otherConverserId])
            .then(conversation => {
                router.push(
                    `/home/conversations/conversation/${conversation.id}`
                );
            })
            .catch(err => {
                if (err.response.status !== 404) {
                    console.error(err);
                }
                getUserById(otherConverserId)
                    .then(userDto => {
                        this._otherConverser = userDto;
                    })
                    .catch(console.error);
            });
    }

    get messages(): Message[] {
        return this._messages;
    }

    get otherConverser(): UserDto {
        return this._otherConverser;
    }

    send(text: string) {
        createConversation(this._otherConverser.id, text)
            .then(conversation => {
                this.eventEmissionConnector.emit(
                    'conversation-created',
                    conversation
                );
                router.push(
                    `/home/conversations/conversation/${conversation.id}`
                );
            })
            .catch(console.error);
    }

    close() {}
}
