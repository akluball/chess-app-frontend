import Conversation, { Message } from './Conversation';
import { UserDto, createUserDtoPlaceholder } from '../../../types/user-types';
import {
    getGameConversation,
    createGameConversationWebSocketUri
} from '../../../data/messaging';
import { MessageDto } from '../../../types/messaging-types';
import store from '../../../store';
import { getUserById } from '../../../data/user';

export default class GameConversation implements Conversation {
    private _messages: Message[] = [];
    private _otherConverser: UserDto = createUserDtoPlaceholder();
    private isWhiteSideUser: boolean | undefined;
    private webSocket: WebSocket | undefined;

    constructor(gameHistoryId: number) {
        getGameConversation(gameHistoryId)
            .then(gameConversationDto => {
                this.isWhiteSideUser =
                    gameConversationDto.whiteSideUserId ===
                    store.getters.currentUserId;
                gameConversationDto.messages.forEach(gameMessageDto => {
                    this.appendMessage(gameMessageDto);
                });
                const otherUserId: number = this.isWhiteSideUser
                    ? gameConversationDto.blackSideUserId
                    : gameConversationDto.whiteSideUserId;
                getUserById(otherUserId)
                    .then(userDto => {
                        this._otherConverser = userDto;
                    })
                    .catch(console.error);
                this.webSocket = new WebSocket(
                    createGameConversationWebSocketUri(gameHistoryId)
                );
                this.webSocket.onopen = this.onOpen.bind(this);
                this.webSocket.onerror = this.onError.bind(this);
                this.webSocket.onclose = this.onClose.bind(this);
                this.webSocket.onmessage = this.onMessage.bind(this);
            })
            .catch(console.error);
    }

    get messages(): Message[] {
        return this._messages;
    }

    get otherConverser(): UserDto {
        return this._otherConverser;
    }

    private onOpen(event: Event) {
        console.log('game conversation websocket open');
    }

    private onError(errorEvent: Event) {
        console.log('game conversation websocket error');
        console.error(errorEvent);
    }

    private onClose(closeEvent: CloseEvent) {
        console.log('game conversation websocket close');
        console.log(closeEvent.reason);
    }

    private onMessage(messageEvent: MessageEvent) {
        this.appendMessage(JSON.parse(messageEvent.data), true);
    }

    private appendMessage(
        messageDto: MessageDto,
        isNewMessage: boolean = false
    ) {
        this._messages.push({
            index: this._messages.length,
            userId: messageDto.userId,
            text: messageDto.text,
            epochSeconds: messageDto.epochSeconds,
            isNewMessage
        });
    }

    send(text: string) {
        if (text.length === 0) {
            return;
        } else if (this.webSocket !== undefined) {
            this.webSocket.send(text);
        } else {
            console.error(
                'unable to send - game conversation websocket not open'
            );
        }
    }

    close() {
        if (this.webSocket !== undefined) {
            this.webSocket.close();
        }
    }
}
