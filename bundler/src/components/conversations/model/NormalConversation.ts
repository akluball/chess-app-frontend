import { MessageDto } from '../../../types/messaging-types';
import {
    createConversationWebSocketUri,
    getConversation
} from '../../../data/messaging';
import { UserDto, createUserDtoPlaceholder } from '../../../types/user-types';
import { getUserById } from '../../../data/user';
import store from '../../../store';
import Conversation, { Message } from './Conversation';
import EventEmissionConnector from '../../../EventEmissionConnector';

export default class NormalConversation implements Conversation {
    private _messages: Message[] = [];
    private _otherConverser: UserDto = createUserDtoPlaceholder();
    private webSocket: WebSocket | undefined;

    private conversationId: number;
    private eventEmissionConnector: EventEmissionConnector;

    constructor(
        conversationId: number,
        eventEmissionConnector: EventEmissionConnector
    ) {
        this.conversationId = conversationId;
        this.eventEmissionConnector = eventEmissionConnector;
        getConversation(conversationId)
            .then(conversation => {
                conversation.messages.forEach(messageDto => {
                    this.appendMessage(messageDto);
                });
                const converserId:
                    | number
                    | undefined = conversation.converserIds.find(
                    converserId => {
                        return converserId !== store.getters.currentUserId;
                    }
                );
                if (converserId !== undefined) {
                    getUserById(converserId)
                        .then(userDto => {
                            this._otherConverser = userDto;
                        })
                        .catch(console.error);
                }
                this.webSocket = new WebSocket(
                    createConversationWebSocketUri(conversationId)
                );
                this.webSocket.onopen = this.onOpen.bind(this);
                this.webSocket.onerror = this.onError.bind(this);
                this.webSocket.onclose = this.onClose.bind(this);
                this.webSocket.onmessage = this.onMessage.bind(this);
            })
            .catch(console.error);
    }

    get messages(): Message[] {
        return this._messages;
    }

    get otherConverser(): UserDto {
        return this._otherConverser;
    }

    private appendMessage(
        messageDto: MessageDto,
        isNewMessage: boolean = false
    ) {
        this.eventEmissionConnector.emit(
            'message',
            this.conversationId,
            messageDto
        );
        this._messages.push({
            index: this._messages.length,
            text: messageDto.text,
            userId: messageDto.userId,
            epochSeconds: messageDto.epochSeconds,
            isNewMessage
        });
    }

    private onOpen(event: Event) {
        console.log('conversation websocket open');
    }

    private onError(errorEvent: Event) {
        console.log('conversation websocket error');
        console.error(errorEvent);
    }

    private onClose(closeEvent: CloseEvent) {
        console.log('conversation websocket close');
        console.log(closeEvent.reason);
    }

    private onMessage(messageEvent: MessageEvent) {
        this.appendMessage(JSON.parse(messageEvent.data), true);
    }

    send(text: string) {
        if (text.length === 0) {
            return;
        } else if (this.webSocket !== undefined) {
            this.webSocket.send(text);
        }
    }

    close() {
        if (this.webSocket !== undefined) {
            this.webSocket.close();
        }
    }
}
