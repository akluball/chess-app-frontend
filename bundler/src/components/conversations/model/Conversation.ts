import { UserDto, createUserDtoPlaceholder } from '../../../types/user-types';

export interface Message {
    userId: number;
    index: number;
    text: string;
    epochSeconds: number;
    isNewMessage: boolean;
}

export default interface Conversation {
    messages: Message[];
    otherConverser: UserDto;
    send(text: string): void;
    close(): void;
}

export function createConversationPlaceholder(): Conversation {
    return {
        messages: [],
        otherConverser: createUserDtoPlaceholder(),
        send() {},
        close() {}
    };
}
