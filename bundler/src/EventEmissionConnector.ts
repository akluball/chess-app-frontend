import Vue from 'vue';

export default class EventEmissionConnector {
    private eventTargets: Vue[] = [];

    addTarget(eventTarget: Vue) {
        this.eventTargets.push(eventTarget);
    }

    emit(eventName: string, ...args: any[]) {
        this.eventTargets.forEach(eventTarget => {
            eventTarget.$emit(eventName, ...args);
        });
    }
}
