import { AxiosRequestConfig } from 'axios';
import Vue from 'vue';
import Vuex, { StoreOptions, Store } from 'vuex';
import { is2xx } from './utils';
import { UserDto } from './types/user-types';
import { getToken, getCurrentUser } from './data/user';

Vue.use(Vuex);

interface ChessAppState {
    token: string | undefined;
    currentUser: UserDto | undefined;
}

let timeoutId: number | undefined;

const storeOptions: StoreOptions<ChessAppState> = {
    state: {
        token: undefined,
        currentUser: undefined
    },
    getters: {
        hasToken(state): boolean {
            return state.token !== undefined;
        },
        currentUserId(state): number | undefined {
            return state.currentUser === undefined
                ? undefined
                : state.currentUser.id;
        },
        bearer(state): string {
            return `Bearer ${state.token}`;
        },
        bearerMixin(state, getters) {
            return (requestConfig?: AxiosRequestConfig) => {
                requestConfig = requestConfig || {};
                requestConfig.headers = requestConfig.headers || {};
                requestConfig.headers['Authorization'] = getters.bearer;
                return requestConfig;
            };
        }
    },
    mutations: {
        setToken(state: ChessAppState, token: string) {
            state.token = token;
            const millisToExpiration =
                1000 * JSON.parse(atob(token.split('.')[1])).exp -
                new Date().getTime();
            timeoutId = window.setTimeout(
                () => store.dispatch('getToken'),
                millisToExpiration - 5
            );
        },
        forgetToken(state: ChessAppState) {
            state.token = undefined;
            if (timeoutId !== undefined) {
                window.clearTimeout(timeoutId);
            }
        },
        setCurrentUser(state: ChessAppState, currentUser: UserDto) {
            state.currentUser = currentUser;
        }
    },
    actions: {
        async getToken({ commit }) {
            const tokenRes = await getToken();
            if (!is2xx(tokenRes)) {
                return;
            }
            const token = tokenRes.data;
            commit('setToken', token);
        },
        async getCurrentUser({ commit }) {
            const currentUser = await getCurrentUser();
            commit('setCurrentUser', currentUser);
        }
    }
};

const store = new Store(storeOptions);

export default store;
