import { AxiosResponse } from 'axios';
import { Children } from 'react';

export function is2xx(response: AxiosResponse): boolean {
    return response.status.toString().startsWith('2');
}

export function secondsToMillis(seconds: number) {
    return seconds * 1000;
}
const IN_MILLISECONDS = {
    DAY: 24 * 60 * 60 * 1000,
    HOUR: 60 * 60 * 1000,
    MINUTE: 60 * 1000,
    SECOND: 1000
};

interface TimeComponents {
    days: number;
    hours: number;
    minutes: number;
    seconds: number;
}

function computeTimeComponents(milliseconds: number): TimeComponents {
    const days = Math.floor(milliseconds / IN_MILLISECONDS.DAY);
    milliseconds -= days * IN_MILLISECONDS.DAY;
    const hours = Math.floor(milliseconds / IN_MILLISECONDS.HOUR);
    milliseconds -= hours * IN_MILLISECONDS.HOUR;
    const minutes = Math.floor(milliseconds / IN_MILLISECONDS.MINUTE);
    milliseconds -= minutes * IN_MILLISECONDS.MINUTE;
    const seconds = Math.floor(milliseconds / IN_MILLISECONDS.SECOND);
    return { days, hours, minutes, seconds };
}

export function toTimerString(milliseconds: number): string {
    if (milliseconds < 0) {
        milliseconds = 0;
    }
    const { days, hours, minutes, seconds } = computeTimeComponents(
        milliseconds
    );
    let timerString = '';
    if (days > 1) {
        timerString += `${days} days, `;
    } else if (days === 1) {
        timerString += `${days} day, `;
    }
    if (hours > 1) {
        timerString += `${hours} hours, `;
    } else if (hours === 1) {
        timerString += `${hours} hour, `;
    }
    let secondsComponent = '' + seconds;
    if (secondsComponent.length === 1) {
        secondsComponent = '0' + secondsComponent;
    }
    timerString += `${minutes}:${secondsComponent}`;
    return timerString;
}

export function isEven(numberToCheck: number): boolean {
    return (numberToCheck & 1) === 0;
}

export function prettifyRatingAdjustment(ratingAdjustment: number): string {
    const magnitude = Math.abs(ratingAdjustment);
    const sign = Math.sign(ratingAdjustment);
    if (sign === 1 || sign === 0) {
        return `+${magnitude}`;
    } else {
        return `-${magnitude}`;
    }
}

export function dateTimeString(epochSeconds: number) {
    const now = new Date();
    const date = new Date(epochSeconds * 1000);
    const minutes =
        date.getMinutes().toString().length === 1
            ? `0${date.getMinutes()}`
            : date.getMinutes();
    let dateTime = `${date.getHours()}:${minutes}`;
    if (
        now.getDate() === date.getDate() &&
        now.getMonth() === date.getMonth() &&
        now.getFullYear() === date.getFullYear()
    ) {
        return dateTime;
    } else {
        dateTime = `${dateTime} on ${date.getMonth() + 1}-${date.getDate()}`;
        if (date.getFullYear() !== now.getFullYear()) {
            dateTime = `${dateTime}-${date
                .getFullYear()
                .toString()
                .slice(-2)}`;
        }
        return dateTime;
    }
}

export function unicode2Html(codePoint: number) {
    return String.fromCharCode(codePoint);
}

export function isInScrollView(
    child: HTMLElement,
    parent: HTMLElement
): boolean {
    if (parent.scrollTop > child.clientTop) {
        return false;
    }
    if (
        parent.scrollTop - parent.scrollHeight <
        child.clientTop - child.clientHeight
    ) {
        return false;
    }
    return true;
}

export function getFontSize(element: HTMLElement) {
    return parseFloat(getComputedStyle(element).fontSize);
}
