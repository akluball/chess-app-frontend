export const FRONTEND_DEPLOYMENTNAME =
    process.env.CHESSAPP_FRONTEND_DEPLOYMENTNAME;
export const GAMEHISTORY_URI = process.env.CHESSAPP_GAMEHISTORY_URI;
export const GAMESTATE_URI = process.env.CHESSAPP_GAMESTATE_URI;
export const MESSAGING_URI = process.env.CHESSAPP_MESSAGING_URI;
export const USER_URI = process.env.CHESSAPP_USER_URI;

export function serviceNameToUri(serviceName: string): string | undefined {
    switch (serviceName) {
        case 'gamehistory':
            return GAMEHISTORY_URI;
        case 'gamestate':
            return GAMESTATE_URI;
        case 'messaging':
            return MESSAGING_URI;
        case 'user':
            return USER_URI;
        default:
            return undefined;
    }
}
