import Vue from 'vue';
import router from './router';
import store from './store';
import './app.css';

Vue.filter('hyphenate', (value: string): string => {
    return value
        .split(' ')
        .map(word => {
            return word.length > 6
                ? word.split('').join(String.fromCharCode(173))
                : word;
        })
        .join(' ');
});

new Vue({
    template: '<router-view></router-view>',
    router,
    store
}).$mount('#app');
