const path = require('path');
const { EnvironmentPlugin } = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const bundleName = 'app.bundle.js';

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    context: path.resolve(__dirname, 'src'),
    entry: {
        app: './app.ts'
    },
    output: {
        path: path.resolve(__dirname, 'build', 'webapp'),
        filename: bundleName
    },
    module: {
        rules: [
            {
                test: /\.tsx?|\.js$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                    configFile: path.resolve(__dirname, 'tsconfig.json')
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }, {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            }, {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
        alias: { vue: 'vue/dist/vue.esm.js' }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src', 'index.html'),
            templateParameters: {
                deploymentName: process.env.CHESSAPP_FRONTEND_DEPLOYMENTNAME,
                bundleName
            },
            inject: false
        }),
        new EnvironmentPlugin([
            'CHESSAPP_FRONTEND_DEPLOYMENTNAME',
            'CHESSAPP_GAMEHISTORY_URI',
            'CHESSAPP_GAMESTATE_URI',
            'CHESSAPP_MESSAGING_URI',
            'CHESSAPP_USER_URI'
        ]),
        new VueLoaderPlugin()
    ]
};
